Home
=======

Welcome to the KSS Wiki! This website is the best source of guides, tutorials, and documentation for the Koronis Scouting System.

Want to contribute? Looking for help? Got a bug to report? Join the KSS [Discord](http://discord.koronis.cc) server!

Overview
-------

Koronis Scouting System is an all-in-one solution to the technical challenges posed for electronic scouting of the FIRST Robotics Competition. Koronis Scouting System, shortened to KSS (pronouced *kAY-Es-Es*), aims to solve multiple problems:

 - **Offline Data Transfer** - FRC events often happen in venues with poor celluar connection, no WiFi networks, and no WiFi hotspots. Because of this, transferring data between devices becomes challenging. KSS provides multiple solutions to transferring data offline:
     - **QR Code Streams** - By utilizing built in cameras in mobile devices, modest amounts of data can be easily transferred through multiple QR codes. Even with low powered devices, data rates average around 200-300 bytes per second. Not enough to transfer high resolution pictures, but enough to easily transfer scouting data.
     - **Audio Cable (libquiet)** - Using quiet.js (libquiet compiled for javascript) to transform data into audio signals, 2KBps transfers through audio cables are possible.
     - **File Based Transfer** - Sometimes, the classic usb drive is the simplest way to transfer data. This option is often the best for data transfer between non-mobile devices, like laptops and desktops.
 - **Internet Enhanced, not Internet Required** - KSS aims to seamlessly integrate with The Blue Alliance (abbreviated as TBA) to provide a public platform for scouting. However, KSS retains offline functionality for the most essential parts of scouting.
 - **Full Scouting Pipeline** - KSS aims to provide full end-to-end solution.
     - **Data Recording** - KSS aims to capture more details beyond surface level statistics. Instead of recording only the counts of robot actions, KSS records each individual action a robot takes with a timestamp and position. This allows for the full reconstruction of robot events.
     - **Data Standarization** - KSS aims to standarize the format within the platform to allow for easier sharing of data between teams.
     - **Data Transfer** - As detailed above, KSS provides multiple ways to transfer data in an offline environment.
     - **Data Processing** - KSS looks to provide tools to extract metrics from data. By providing a platform to run user generated code to extract data in a user customizable way, KSS aims to provide a flexible and powerful way for the user to gain deep insight into robot performance.
     - **Data Visualization** - KSS looks to provide two avenues to create visual representations of metrics.
        1) Simple and standardized charts and graphs for simple metrics. This allows for a faster and easier way of data visualization from custom metrics.
        2) Full custom visualizer for limitless visualization. KSS provides a blank canvas (literally a `<canvas>` tag) and some common JS libraries (Chart.js, plotly.js, d3.js, etc.) to allow for complete visualization customization.
 - **Cross Platform Compatibility** - Built on common web technologies and using powerful PWA frameworks, KSS aims for broad compatibility between different platforms and operating systems. *Note: Due to Apple refusing to allow 3rd party web engines and forcing all 3rd party web browsers (like Chrome for iOS) to use their outdated WebKit web engine, KSS works only in an limited fashion on iOS. See the [iOS Support](faq/ios_support.md) page for more information*
 - **Open Source Software** - All source code is available under AGPLv3 license at [https://gitlab.com/koronis-scouting-system/](https://gitlab.com/koronis-scouting-system/). KSS is designed to be fully transparent and freely available to everyone.