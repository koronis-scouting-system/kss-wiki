iOS Support
=======

Time of writing: 08/11/2020

Apple as a company has create policies surrounding their iOS platform that makes it impossible to promise complete functionality of KSS as a web app from a technical standpoint. 

At the current time of writing, Apple has numerous polices preventing many of the web technologies that KSS uses to function offline:

 1) Apple prevents 3rd party web browsers (like Chrome for iOS) from using 3rd party web engines. Instead, Apple forces the iOS WebKit web engine as the only web engine allowed on the  platform.
 2) The iOS WebKit engine does not allow for camera or audio recording (KSS needs these for receiving data offline) when in offline, standalone mode.
 3) Apple does not plan to implement the web bluetooth standard which is required for KSS Beacon data transfer. See https://webkit.org/status/#feature-web-bluetooth

Because of this, KSS as a web app can only support limited funtionality on the iOS platform. 

Why Not Native Apps?
-------

When KSS started, it was only one developer working on the project. As a single developer, the author did not have the time to create a full suite of native apps. A possible solution is to use mobile frameworks that compile into native apps (Cordova, Ionic, etc.). However, the amount of work required and the deadline following (targeting 2020 season) meant the author did not have the time to configure a CI/CD pipeline for native apps. Currently, KSS utilizes cutting edge web technologies that may not be possible to easily transfer over to native apps.

When KSS reaches a mature state or more developers join the project, native apps might be possible in the future.

Resolution
-------

Unless Apple changes these policies or until native apps are created, it is technically impossible for KSS as a web app to function offline fully.

A possible resolution may be to use either school provided laptops to run KSS or cheap Android phones (LG Rebel 3 phones have been used to test KSS).
