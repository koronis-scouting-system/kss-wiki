Why Koronis?
=======

The name Koronis Scouting System was chosen to reflect the idea of gathering intelligence. Koronis is derived from Coronis, which stands for 'crow' or 'raven' in Ancient Greek. The name also has a faint homage to the CORONA spy satellite program.

An Homage to the CORONA spy satellite
-------

The CORONA spy satellite program was created during the late 1950s. The US Air Force commissioned the first American spy satellite program to gather photo intelligence in space. The program had to solve the main technical problem of how to transfer photos from space to earth in an offline environment (the technology to wirelessly transfer high quality photos from space was not reliable at that time). CORONA came to create a system in which after the photo cartidge was full of photo intelligence, it was inserted into a small reentry vehicle. The main satellite would then pitch down to launch the reentry vehicle down back to earth. After the reentry vehicle entered the atmosphere, it would deploy a parachute to slow down the vehicle. An airplane would then use a hanging hook to catch the vehicle's parachute and recover the data.

![KH-4B CORONA satellite diagram](https://upload.wikimedia.org/wikipedia/commons/c/c6/Kh-4b_corona.jpg "The KH-4B CORONA satellite.")
![CORONA film recovery maneuver diagram](https://upload.wikimedia.org/wikipedia/commons/f/f4/CORONA_film_recovery_maneuvar.jpg "CORONA film recovery maneuver.")

KSS itself was created to solve the similar problem of data transfer in an offline environment and as such, it was named with a faint homage to the CORONA program.