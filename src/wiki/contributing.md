Contributing
=======

In order to contribute to the wiki, you must have a GitLab account to modify the files (https://gitlab.com/users/sign_up).

After signing up for a GitLab account, click on the 'Edit' section in the top navigation bar in the wiki (https://wiki.koronis.cc/).

Wiki Documentation
-------

KSS Wiki uses the Markdown language to display information (https://www.markdownguide.org/getting-started/).

The main site is using MDWiki to publish the KSS Wiki as a website (http://dynalon.github.io/mdwiki/#!quickstart.md).

The wiki on KSS Client directly pulls from the raw markdown data to display the information as formatted HTML (https://github.com/rexxars/react-markdown).

Note: You can easily preview the Markdown changes using the 'Preview Markdown' tab in the GitLab IDE.

Creating New Pages
-------

To create a new page, create a new file with the .md extension within the src folder. Example: `src/dev/testing.md`

Note: When using the GitLab IDE, the 'New File' button is located in the side menu on the left (to the right of the 'Edit' text)

In order for MDWiki and KSS Client to find the new page, the `src/navigation.md` file needs to be modified to include the new file.

When editing the `src/navigation.md` file, please refer to http://dynalon.github.io/mdwiki/#!quickstart.md#Adding_a_navigation on how to format navigation links. The navigation link must lie somewhere between the `# KSS Wiki` and `[Edit]` line. Note that the link path excludes 'src/' (Example: `filename: src/dev/testing.md -> nav link: dev/testing.md`).

Submitting Changes
-------

To submit changes within the GitLab IDE interface, click on the 'Commit...' button. Before clicking on the 'Commit' button, make sure the 'Start a new merge request' checkbox is checked.

GitLab should redirect you from the IDE to a new form to submit merge requests. There is no need to add a description or modify the title, but adding descriptive text is appreciated. Before clicking on the 'Submit merge request', make sure the 'Delete source branch when merge request is accepted' checkbox is checked.

After submitting the changes, someone will review the changes and approve or decline the new changes. This process may take awhile depending on availability of people to review changes.
