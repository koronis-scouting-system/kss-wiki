App Bar
-------

![App Bar](assets/quick_overview01.png)

The App Bar contains a hamburger menu, title, and login button. Currently the login button is unused, so the only thing of note is the hamburger menu. This menu shows all the other sections of the app.

Side Menu

![Side Menu](assets/quick_overview02.png)

The Side Menu contains links to all the sections of the app. Sliding from left to right will not work, as this may interfere with recording.

Home
-------

![Home](assets/quick_overview03.png)

Currently the home page does not contain more content. In the future, most important information will be displayed here.

Record
-------

![Record](assets/quick_overview04.png)

This section contains a list of records and a button to start recording a new match.

Note: Screenshot above is slightly out of date.

Process
-------

![Process](assets/quick_overview05.png)

This section contains a process editor and executor.

Analyze
-------

![Analyze](assets/quick_overview06.png)

This section contains metrics and analytics.

Transfer
-------

![Transfer](assets/quick_overview07.png)

This section allows for sending and receiving data.

Next
-------

Let's look at the [architecture](architecture.md) of KSS.