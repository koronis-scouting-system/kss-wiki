Terminology
-------

Here are some terms used in this system:

  - Record - This represents one scouter recording one robot in one match. This is the basic building block of the system.
  - Process - This represents a code fragment that turns record(s) into usable metrics or visuals. Metrics contain a query type and data type to signify what the process can do.

Possible query types:

  - Record - Processes with a query type of 'record' only processes one record.
  - Match - Processes with a query type of 'match' only records pertaining to one match.
  - Team - Processes with a query type of 'team' process all records pertaining to one team.
  - Event - Processes with a query type of 'event' process all records pertaining to one event.

Possible data types:

  - Metric - Processes with a data type of 'metric' returns a single numerical value (return NaN if not applicable).
  - Array - Processes with a data type of 'array' returns an array of numerical values. Disabled currently since tooling for this is not available yet.
  - Chart - Processes with a data type of 'chart' transforms a given HTML div element and creates a chart.

Example:

A process with a query type of 'record' and data type of 'metric' could for example, return the 'total number of power cells scored' or 'time it takes to move off the auto line'.

Next
-------

First step of scouting with KSS is [recording](recording.md) data.