Installation
=======

Installation is fairly straight forward. Instructions for devices are found below.
Officially supported platforms:

  - Chrome or Chromium on Windows, Mac, and Linux
  - Chrome on Android

In theory, any browser running the blink engine should work, so Mac computers should be compatible as well (your mileage may vary). Firefox and other browsers are untested, so the web app may not work with full functionality. Safari is known to not work well in data gathering. Note, Chrome on iOS is just a poorer version of Safari, so best experience is to use Safari on iOS.

Browser
-------

 1. Make sure the latest Chrome or Chromium browser is installed
 2. Go to [https://app.koronis.cc](https://app.koronis.cc)
 3. This webpage should work even if offline, navigate back to this link use the app
 4. You're all set!

Windows, Mac, and Linux
-------

For people looking for a near native application experience, follow the steps below.

 1. Make sure the latest Chrome or Chromium browser is installed
 2. Go to [https://app.koronis.cc](https://app.koronis.cc)
 3. Look for the install / download icon button in the url bar

![Url bar install icon button](assets/installation01.png)

 4. Click install

![Install popup](assets/installation02.png)

 5. Desktop link should appear

![Desktop link](assets/installation03.png)

 6. You're all set!

Android
-------

For people looking for a near native app experience, follow the steps below.

 1. Make sure the latest Chrome browser is installed
 2. Go to [https://app.koronis.cc](https://app.koronis.cc)
 3. Open the top right settings menu

![Android Chrome settings menu](assets/installation04.png)

 4. Select the 'Add to Home screen'

![Add to Home screen popup](assets/installation05.png)

 5. Select Add
 6. The icon should appear on your home screen, you're all set!

iPhone
-------

 1. iPhone is not officially supported, as Apple has restricted much of the PWA technologies on their browsers
 2. Best experience is to use the latest version of Safari (Chrome on iPhone is just a worse version of Safari, because Apple disallows non-webkit web engines from running)
 3. Go to [https://app.koronis.cc](https://app.koronis.cc)
 4. Click on the share button
 5. Look for 'Add to Home screen'
 6. Confirm the installation
 7. The icon should appear on your home screen, you're all set!

Updating
-------

For versions 1.2.10 and above:

 1. Navigate to the home page.
 2. Refresh the web page (may need to close and open if using near native app).
 3. Check for 'New Update Available!' button under the Version section.
 4. Click on the button. KSS will automatically update to the latest version.

For older versions:

 1. Open the app
 2. Close the app and all tabs containing the webpage
 3. Reopen the app
 4. Repeat until updated

Note: When closing the app and tabs, all https://app.koronis.cc pages should be closed.

Next
-------

Let's look at a [quick overview](quick_overview.md) of KSS.