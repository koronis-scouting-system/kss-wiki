Getting Started
=======

Before using Koronis Scouting System, make sure you have the right hardware.

KSS requires each scouter to use a device to scout (preferrably a mobile device).
Since each device will need to go through the installation process, make sure to plan accordingly (internet is required for that step).

In addition to each scouter needing a device, its recommended that a large screen device (preferrably a laptop) is used as a centralized drop off point.
This can also double as an analysis workstation.

KSS has multiple ways to transfer data, but some may require more preparations than others:

  - **QR Code Streams** - Almost no preparations are required, but some devices (like laptop) may benefit from an external webcam. This is also the slowest option, but also requires the least amount of preparations. Note: webcams may require focal length adjustment before use.
  - **Audio Cable (libquiet)** - Requires preparing an audio cable that can connect from speaker output into microphone input. On devices with combined input/output audio jack, this may require a splitter.
  - **File Based Transfer** - Requires preparing a way to file transfer, like a USB flash drive. This may require extensive preparation and testing on some devices (like mobile devices).

Example Setup
-------

Below is an example setup that a team might bring to competition.

  - 1x Laptop
  - 1x Audio cable with splitter
  - 1x Large powerbank (20,000+ mAh)
  - 6x Android tablets

Since KSS works on nearly all devices, a smartphone only setup is possible (testing and practice is recommended).


Next
-------

Before you can start using KSS, it needs to be [installed](installation.md) first! 