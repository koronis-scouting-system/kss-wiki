Recording
=======

Recording Screen Overview
-------

Note: The images below indicate the 2020 season specific setup, some elements may change year to year.

Note: It's recommended to use devices with touch capability to record (phones or tablets) as it is more intuitive than using keyboard + mouse.

To start the recording process, go to the record page.

Note: Screenshot below is slightly out of date.

![Record page](assets/recording01.png)

Click on the record tab on the bottom.

![Match info](assets/recording02.png)

Input information about the match and save. You can go back to this section later. Switch your phone to landscape mode.

Note: Match number for playoff matches should be the match in the series (since each round is best 2 out of 3, match number will normally be 1, 2, or 3).

![image](assets/recording03.png)

Note the top bar. This section contains the following:

![image](assets/recording04.png)

  - Flip button - Use this if you are seated on the opposite side
  - Line indicator - This indicates the bot is on the auto line (game specific)
  - Power Cell indicator - This indicates the amount of power cells (game specific)
  - Time indicator - This shows the amount of time elapsed since match start
  - Play/Pause - Start and pause recording (on keyboard, spacebar can be used to Play/Pause)
  - Edit Info - Edit match information
  - Save - Save the match, currently this will also exit the recording engine
  - Settings - Configure update interval, button stack width, and year to scout
  - Restart/Close - Restart or close the recording

Note the field. This is where you indicate robot position through touch or mouse.

![image](assets/recording05.png)

Note the button stack. This is where you indicate robot events. The letter of a button surrounded by parentheses is the letter on the keyboard used to trigger events.

![image](assets/recording06.png)

How To Record Matches
-------

#### Pre-Match Preparation

Before the match starts, input match information such as robot number and match number. Use

When recording:

  - If using touch controls, use the right hand to indicate robot position. Use the left hand to trigger events.
  - If using keyboard + mouse controls (not recommended), use the mouse to indicate robot position. Use the keyboard to trigger events.

Exact position accuracy is not expected.

Tips
-------

  - When recording, prioritize event accuracy first, then event timings, then position accuracy.
  - Hold the phone up to the field to minimize looking down.
  - Holding down on a button prepares for the event to trigger, releasing is when the event is actually recorded. If you slide your finger out of the button box, the event is cancelled.