Introduction to KSS Packages
=======

Because the FIRST Robotics Competition changes rules each year, most scouting systems need extensive redesigning before they can become operational. KSS solves this by utlizing a "package" system, in which new game rules can be defined in a standardized way. This means quick turn around time from when new rules are released to when KSS is available for the new game, allowing teams to focus on deeper data analytics problems.

Overview
-------

KSS Packages contain 7 different files that define each year's game:
 - `bot.js` - Contains `botStateDefinition`, which defines various game specific properties of robots for use in button hiding logic, automatic event emitting, and status displays. Exported ES6 module style.
 - `button.js` - Contains `buttonStateDefinition`, which is an array of objects defining custom buttons used to emit events for the event log in a record. Exported ES6 module style.
 - `color.json` - Defines several color palettes for ui elements like the field and buttons.










