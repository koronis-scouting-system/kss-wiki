Testing
=======

This is the list of actions to test before release

Home
-------

 * Page displays without crashing
 * Fullscreen modal shows

Record
-------

 * Page displays without crashing
 * Record can be displayed
 * Filter works (all components)
 * Records are able to be deleted

Record Engine
-------

 * Record Engine displays without crashing
 * All events are able to be generated
 * Can flip colors
 * Can flip sides
 * Save records correctly
 * Exits correctly

Process
-------

 * Process displays without crashing
 * Can open, new, save, and save as new correctly
 * Can execute metrics and charts

Analyze
-------

 * Analyze displays without crashing
 * Exports data
 * Filter records
 * Select processes

Transfer
-------

 * Transfer displays without crashing
 * QRCode, Audio, and Beacon (*WIP*) work correctly
 * Send, receive, and share works correctly
 * Transfers both records and processes

Security
-------

 * Verification process works
 * No user secret leakage
