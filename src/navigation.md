# KSS Wiki

[Main](index.md)

[Tutorials]()

  * [Getting Started](tutorial/getting_started.md)
  * [Installation](tutorial/installation.md)
  * [Quick Overview](tutorial/quick_overview.md)
  * [Architecture](tutorial/architecture.md)
  * [Recording](tutorial/recording.md)

[Docs]()
  
  * # Wiki
  * [Contributing](wiki/contributing.md)
  * # Development
  * [Development Convention](dev/dev_conv.md)
  * [Testing](dev/testing.md)

[FAQ]()

  * [iOS Support](faq/ios_support.md)
  * [Why Koronis?](faq/why_koronis.md)

[Edit](https://gitlab.com/-/ide/project/koronis-scouting-system/kss-wiki/blob/master/-/src/wiki/contributing.md)
[gimmick:theme](cosmo)
